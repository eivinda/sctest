#include <stdlib.h>
#include "sctest.h"

TEST(#include "fake_src.h")

int
one()
{
	return 2; /* Ups. */
}

TEST(
void
test_one(void)
{
	int tmp;
	ASSERT_P( (tmp = one()) == 1, "one() returned %d.", tmp );
}
)


void *
fail_malloc()
{
	return NULL;
}

TEST(
void skiping_test( void ) {
	char *ptr = fail_malloc();
	ASSERT_R( ptr != NULL );
	ASSERT( ptr[0] == 0 ); /* This will be skipped if previous test fails. */
}
)

TEST(void passing_test(void)
{
	ASSERT( 2 + 2 == 4 );
})
