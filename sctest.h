#pragma once

/* Skip all tests. */
#define TEST( ... ) /* NoOp. */

#ifdef PERFORM_TESTS
/* Perform tests. Include boilerplate. */
#include <stdio.h>

extern int sctest_success;
extern int sctest_fails;

#define NONE   "\e[0m"
#define RED    "\e[31m"
#define GREEN  "\e[32m"

#define ASSERT_P( cond, fmt, ... ) do {                                        \
	if ( cond ) {                                                          \
		++sctest_success;                                              \
	} else {                                                               \
		++sctest_fails;                                                \
		fprintf( stderr, "%s: " RED "%s" NONE " failed. " fmt "\n",    \
				__func__, #cond, ##__VA_ARGS__ );              \
	}                                                                      \
} while ( 0 )

#define ASSERT_R( cond, ... ) do {                                             \
	if ( cond ) {                                                          \
		++sctest_success;                                              \
	} else {                                                               \
		++sctest_fails;                                                \
		fprintf( stderr, "%s: " RED "%s" NONE " failed." RED           \
				" Skipping test.\n" NONE, __func__, #cond );   \
		return __VA_ARGS__;                                            \
	}                                                                      \
} while ( 0 )

#define ASSERT( cond ) ASSERT_P( cond, "" )

#endif
