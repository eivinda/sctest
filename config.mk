# Paths.
PREFIX ?= /usr/local
MANPREFIX := ${PREFIX}/share/man

# Flags.
CFLAGS += -Wall -std=c99 -D_POSIX_C_SOURCE=200809L
