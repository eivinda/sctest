include config.mk

SRC    := sctest.c
OBJS   := ${SRC:.c=.o}
TARGET := sctest

all : ${TARGET}

clean :
	@rm -r *.o ${TARGET} fake_test.c fake_test 2> /dev/null || true

${TARGET} : ${OBJS}
	${CC} ${LDFLAGS} ${OBJS} -o ${TARGET}

README : ${TARGET}.1
	groff -man -Tascii ${TARGET}.1 | col -b -x > README

fake_test : fake_test.o fake_src.o
	@${CC} ${LDFLAGS} fake_test.o fake_src.o -o $@
	@echo Running fake_test
	@./fake_test || true

fake_test.c : fake_src.c ${TARGET}
	./${TARGET} -mo $@ fake_src.c

install : ${TARGET}
	@echo Installing executable ${DESTDIR}${PREFIX}/bin/${TARGET}
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp ${TARGET} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${TARGET}
	@echo Installing headerfile ${DESTDIR}${PREFIX}/include/sctest.h
	@mkdir -p ${DESTDIR}${PREFIX}/include
	@cp sctest.h ${DESTDIR}${PREFIX}/include
	@chmod 644 ${DESTDIR}${PREFIX}/include/sctest.h
	@echo Installing manpage ${DESTDIR}${MANPREFIX}/man1/${TARGET}.1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@cp ${TARGET}.1 ${DESTDIR}${MANPREFIX}/man1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/${TARGET}.1

uninstall :
	@rm ${DESTDIR}${PREFIX}/bin/${TARGET}
	@rm ${DESTDIR}${PREFIX}/include/sctest.h
	@rm ${DESTDIR}${MANPREFIX}/man1/${TARGET}.1

.PHONY : all clean install uninstall
